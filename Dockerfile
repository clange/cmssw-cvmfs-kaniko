FROM gitlab-registry.cern.ch/cms-cloud/cmssw-docker/cc7-cms


ARG CMS_PATH=/cvmfs/cms.cern.ch
ARG CMSSW_RELEASE=CMSSW_10_6_8_patch1
ARG SCRAM_ARCH=slc7_amd64_gcc700

SHELL ["/bin/bash", "-c"]

WORKDIR /code

RUN shopt -s expand_aliases && \
    set +u && \
    source ${CMS_PATH}/cmsset_default.sh; set -u && \
    cmsrel ${CMSSW_RELEASE} && \
    cd ${CMSSW_RELEASE}/src && \
    cmsenv && \
    mkdir ${CMSSW_BASE}/src/AnalysisCode

ADD ZPeakAnalysis /code/${CMSSW_RELEASE}/src/AnalysisCode/ZPeakAnalysis

RUN shopt -s expand_aliases && \
    set +u && \
    source ${CMS_PATH}/cmsset_default.sh; set -u && \
    cd ${CMSSW_RELEASE}/src && \
    cmsenv && \
    scram b
